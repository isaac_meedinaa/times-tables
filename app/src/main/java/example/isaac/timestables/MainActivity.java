package example.isaac.timestables;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class MainActivity extends AppCompatActivity {

    ListView lv1;
    SeekBar skb1;
    int max = 20;
    int startingNumber = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Seekbar
        skb1 = (SeekBar) findViewById(R.id.skb_one);

        // ListView
        lv1 = (ListView) findViewById(R.id.lv_one);

        // function
        skb1.setMax(max);
        skb1.setProgress(startingNumber);
        generateMultiple(startingNumber);
        sliderMultiple();
    }

    public void sliderMultiple() {
        skb1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                int min = 1;
                int timesTableNumber;

                if (i < min) {
                    timesTableNumber = min;
                    skb1.setProgress(min);
                } else {
                    timesTableNumber = i;
                }

                generateMultiple(timesTableNumber);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    public void generateMultiple(int timesTableNumber) {
        ArrayList<String> timesTableContent = new ArrayList<String>();

        for (int j = 1; j <= 200; j++) {
            timesTableContent.add(Integer.toString(j * timesTableNumber));
        }

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, timesTableContent);
        lv1.setAdapter(arrayAdapter);
    }
}
